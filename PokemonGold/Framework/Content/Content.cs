﻿using PokemonGold.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Framework.Content
{
    public class Content
    {
        /// <summary>
        /// Läser in en bild och omvandlar den till en Texture2D
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Texture2D Load(string path)
        {
            Bitmap bitmap = new Bitmap(path);

            // den  här metoden ska ta reda på pixlarna inom bilden vi vill ladda in
            byte[] pixels = ContentManager.ImageToChar(bitmap);

            Texture2D texture = new Texture2D(bitmap.Width, bitmap.Height, pixels);
            bitmap.Dispose();

            return texture;
        }

        public static void FromGrayimageToColorimage(string path, GrayToColorMode mode)
        {
            Bitmap loadedBitmap = new Bitmap(path);
            Bitmap savedBitmap = new Bitmap(loadedBitmap.Width, loadedBitmap.Height);

            Color[] newColors = ContentManager.GrayscaleToColor(
                ContentManager.ColorToGrayscale(ContentManager.GetAllPixels(loadedBitmap)),
                mode);


            for (int y = 0; y < savedBitmap.Height; y++)
                for (int x = 0; x < savedBitmap.Width; x++)
                    savedBitmap.SetPixel(x, y,newColors[y * savedBitmap.Width + x]);


            string savePath = path.Remove(path.Length - 4, 4) + "_AsColor_" + mode.ToString() + ".png";

            savedBitmap.Save(savePath);

            loadedBitmap.Dispose();
            savedBitmap.Dispose();
        }
    }
}
