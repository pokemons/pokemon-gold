﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Framework.Content
{
    /// <summary>
    //Den här klassen ska ta hand om det logiska som ska ske
    //vi tar en bild , gör till array med alla pixlar
    //sedan tar vi alla pixlar och gör dem till svartvita pixlar
    /// </summary>
    class ContentManager
    {
        private static char[] Pixels = { '@', '%', '&', 'M', '$', '@', '#', 'x', '#', 'O', ')', ')', '+',  'n',  '8', 'c', 'c', 'c', '!', '+', '*', '\"', '.', '\'' };

        //Med hjälp av värdena så kan vi avgöra styrkan, går att ändra och test nya värden. Summan av (bwR + bwG + bwB) MÅSTE bli 1
        private static float bwR = 0.2410f;
        private static float bwG = 0.6232f;
        private static float bwB = 0.1268f;


        /// <summary>
        /// Gör om en Bitmap till en array med char
        /// </summary>
        public static byte[] ImageToChar(Bitmap bitmap)
        {
            Color[] colors = new Color[bitmap.Width * bitmap.Height];

            //Gå igenom all pixlar i bitmapen och spara dem i en array
            for (int y = 0; y < bitmap.Height; y++)
                for (int x = 0; x < bitmap.Width; x++)
                    colors[y * bitmap.Width + x] = bitmap.GetPixel(x, y);

            //Omvandlar våran array med färger till en array med svartvita(gråa) färger
            int[] grayPixels = ColorToGrayscale(colors);

            //Går igenom alla svartvita värden och omvandlar det till en char
            byte[] charPixels = new byte[grayPixels.Length];
            for (int i = 0; i < charPixels.Length; i++)
                charPixels[i] = ByteToChar(grayPixels[i]);

            return charPixels;
        }

        public static Color[] GetAllPixels(Bitmap bitmap)
        {
            Color[] colors = new Color[bitmap.Width * bitmap.Height];

            for (int y = 0; y < bitmap.Height; y++)
                for (int x = 0; x < bitmap.Width; x++)
                    colors[y * bitmap.Width + x] = bitmap.GetPixel(x, y);

            return colors;
        }

        /// <summary>
        /// Tar in en array med färger gör om dem till svartvita värden
        /// </summary>
        public static int[] ColorToGrayscale(Color[] colors)
        {
            int[] pixels = new int[colors.Length];

            for (int i = 0; i < colors.Length; i++)
            {
                //När alphan är 0 (pixeln är genomskinlig) sätter vi våran svartvita värde till 999 som räknas som genomskinlig
                if (colors[i].A == 0)
                    pixels[i] = 999;
                else
                    pixels[i] = (byte)(colors[i].R * bwG + colors[i].G * bwR + colors[i].B * bwB); //Här omvandlas vår färg till en svarvit färg
            }

            return pixels;
        }

        /// <summary>
        /// Tar in en gråskala och försöker göra om den till en färg
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static Color[] GrayscaleToColor(int[] grayColors, GrayToColorMode mode)
        {
            Color[] newColors = new Color[grayColors.Length];

            float multiplier = 1f / ((mode == GrayToColorMode.GTCr) ? bwR : (mode == GrayToColorMode.GTCg) ? bwG : bwB);
            float fill = 255f / (1f / multiplier * 100f);

            for (int i = 0; i < grayColors.Length; i++)
            {
                if (grayColors[i] == 999)
                {
                    newColors[i] = Color.Transparent;
                    continue;
                }

                if (grayColors[i] > 3)
                {

                }

                int newR = (int)(multiplier / bwR * (grayColors[i] * multiplier * fill * bwR));
                int newG = (int)(bwG / multiplier * (grayColors[i] * multiplier * fill * bwG));
                int newB = (int)(bwB / multiplier * (grayColors[i] * multiplier * fill * bwB));
                if (newR > 255) newR = 255;
                if (newG > 255) newG = 255;
                if (newB > 255) newB = 255;

                newColors[i] = Color.FromArgb(newR, newG, newB);
            }

            return newColors;
        }

        /// <summary>
        /// Tar in en int (0-255) och gör om den till en char.
        /// Om man skickar in (999) så kommer ett mellanslag att retuneras
        /// </summary>
        public static byte ByteToChar(int b)
        {
           //När (b) är 999 så räknas det som en genomskinlig pixel; så retunera ' '
            if (b == 999)
                return 32; // 32 = mellanslag

            //Här vill vi få fram hur "mörk" vi vill ha utifrån våran array med pixels
            //i kommer alltid att bli ett värde mellan (0 och Pixels.Length - 1)
            int i = (int)((b / 256f) * Pixels.Length);

            return (byte)Pixels[i];
        }
    }

    public enum GrayToColorMode
    {
        GTCr, // Gray To Color - red as dominant
        GTCg, // Gray To Color - green as dominant
        GTCb  // Gray To Color - blue as dominant
    }
}
