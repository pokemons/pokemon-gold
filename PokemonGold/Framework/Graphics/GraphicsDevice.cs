﻿using PokemonGold.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Framework.Graphics
{
    public class GraphicDevice
    {
        //Första arrayen beskriver Y sedan den andra arrayen X
        public static byte[][] Buffer; //En "lista" med pixlar

        public static int BufferWidth, BufferHeight, //Värden till bufferwtorleken
                          ScreenWidth, ScreenHeight; //värden till fönster storleken

        /// <summary>
        /// Retunerar punken som ligger i mitten av skärmen
        /// </summary>
        public static Point ScreenCenter { get { return new Point(ScreenWidth / 2, ScreenHeight / 2); } }

        private static int bufferSize; //Beskriver storleken på buffern (BufferWidth * BufferHeight)

        private static BufferedStream stream; //Skapar en ny bufferStream, den används för att skriva ut alla pixlar

        public static bool Init(int width, int height)
        {
            bool succeeded = true;

            //vi vill ha våran buffer dubbel storlek
            BufferWidth = width * 2;
            BufferHeight = height;

            ScreenWidth = width;
            ScreenHeight = height;

            //Försök att sätta höjden på fönstret till den önskade höjden, om det inte går så sätt höjden på fönstret till den maximala höjdstorleken
            try
            {
                Console.WindowHeight = BufferHeight;
                Console.BufferHeight = BufferHeight;
            }
            catch
            {
                succeeded = false;

                int maxHeight = Console.LargestWindowHeight;
                Console.WindowHeight = maxHeight;
                Console.BufferHeight = maxHeight;

                ScreenHeight = maxHeight;
                BufferHeight = maxHeight;
            }

            //Försök att sätta bredden på fönstret till den önskade bredden * 2, om det inte går så sätt bredden på fönstret till den maximala breddstorleken
            try
            {
                Console.WindowWidth = ScreenWidth * 2;
                Console.BufferWidth = ScreenWidth * 2;
            }
            catch
            {
                succeeded = false;

                int maxWidth = Console.LargestWindowWidth / 2;
                Console.WindowWidth = maxWidth * 2;
                Console.BufferWidth = maxWidth * 2;

                ScreenWidth = maxWidth;
                BufferWidth = maxWidth * 2;
            }

            //Bufferstorleken är bredden * höjden
            bufferSize = BufferWidth * BufferHeight;

            Buffer = new byte[BufferHeight][];

            for (int y = 0; y < BufferHeight; y++)
                //För varje y axel så skapas en array med width värdet, alltså bredden.
                Buffer[y] = new byte[BufferWidth];

            //Kallar Clear() metoden för att sätta varje pixel i buffern till "space"
            ClearBuffer();

            //Sätter vår text Encoding till UTF8 (det finns fler alternativ, men UTF-8 är den snabbaste. Man kan dock inte skriva Å.Ä,Ö med UTF8)
            Console.OutputEncoding = Encoding.UTF8;
            //Länkar våran BufferStream till consolens BufferStream
            stream = new BufferedStream(Console.OpenStandardOutput(), bufferSize);

            return succeeded;
        }

        /// <summary>
        /// Rensar hela buffern
        /// </summary>
        public static void ClearBuffer()
        {
            //Här skapar vi en for loop som "nollar" alla pixlar till "space".
            for (int y = 0; y < BufferHeight; y++)
                for (int x = 0; x < BufferWidth; x++)
                    Buffer[y][x] = 32; // 32 = mellanslag
        }

        /// <summary>
        /// Rensar (x, y) pixeln i buffern
        /// </summary>
        public static void ClearBuffer(int x, int y)
        {
            //Rensa bara pixeln när vår x och y är innanför fönstret
            if (x >= 0 && x < BufferWidth &&
                y >= 0 && y < BufferHeight)
            {
                Buffer[y][x] = 32; // 32 = mellanslag
            }
        }

        /// <summary>
        /// Skriver en pixel (c) till den angivna positionen (x, y) i buffern
        /// </summary>
        public static void DrawToBuffer(byte c, int x, int y)
        {
            //Får inte skriva ett mellanslag direk till buffern, använd Clear(x, y)
            if (c == 32) // 32 = mellanslag
                return;

            //Rita våran pixel till buffern bara när den ligger innanför fönstret
            if (x >= 0 && x < BufferWidth &&
                y >= 0 && y < BufferHeight)
            {
                Buffer[y][x] = c;
            }
        }

        /// <summary>
        /// Skriver ut hela buffern till skärmen
        /// </summary>
        public static void Render()
        {
            for (int y = 0; y < BufferHeight; y++)
                //Skriver till vår BufferedStream för att den är 100 ggr snabbare än Console.Write()
                stream.Write(Buffer[y], 0, BufferWidth);
        }
    }
}
