﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Framework.Graphics
{
    /// <summary>
    /// Den här klassen tar hand om allt som ska ritas.
    /// </summary>
    public class SpriteBatch
    {
        public static float Scale = 1f;
        /// <summary>
        /// Skriver ut en text vid den angivna positionen
        /// </summary>
        /// <param name="text">Texten som ska skrivas ut</param>
        /// <param name="position">Positionen som texten ska skrivas ut</param>
        //public static void DrawString(string text, Point position)
        //{
        //    //Den här metoden gör så man kan skapa en sträng som kan flyga runt some ne galning !
        //    // Istället för bara en char
        //    for (int i = 0; i < text.Length; i++)
        //        GraphicDevice.DrawToBuffer(text[i], position.X + i, position.Y);
        //}

        /// <summary>
        /// Tar bort pixlar på skärmen vid positionen (position.Position) och storleken (position.Size)
        /// </summary>
        public static void DrawClear(Rectangle position)
        {
            //Här går vi igneom varje Y pixel
            for (int y = 0; y < position.Height; y++)
            {
                int xOffset = 0;
                //Sedan vill gå igenom alla inom X pixel
                for (int x = 0; x < position.Width; x++)
                {

                    //Dem här skapar "riktiga" pixlar, vi vill med dessa göra så pixlarna är jämna och fina.
                    //Därför lägger vi in på x axeln att vi vill skicka in 2 tecken bredvid varandra 
                    //Eftersom tecknet är dubbelt så lång som brett.
                    GraphicDevice.ClearBuffer(position.X * 2 + xOffset + x, position.Y + y);
                    GraphicDevice.ClearBuffer(position.X * 2 + xOffset + x + 1, position.Y + y);

                    // För att inte skriva över samma pixel skapar vi en varie
                    xOffset++;
                }
            }
        }

        /// <summary>
        /// Ritar en pixel (c) vid (position)
        /// </summary>
        public static void DrawPixel(char pixel, Point position)
        {
            byte convertedPixel = (byte)pixel;

            GraphicDevice.DrawToBuffer(convertedPixel, position.X * 2, position.Y);
            GraphicDevice.DrawToBuffer(convertedPixel, position.X * 2 + 1, position.Y);
        }

        public static void DrawOutlinedRect(char texture, Rectangle position, int outlineWidth)
        {
            DrawRect(texture, new Rectangle(position.Position, new Point(position.Width, outlineWidth))); // Top
            DrawRect(texture, new Rectangle(position.Position, new Point(outlineWidth, position.Height))); //Left
            DrawRect(texture, new Rectangle(new Point(position.Right - outlineWidth, position.Y), new Point(outlineWidth, position.Height))); // Right
            DrawRect(texture, new Rectangle(new Point(position.X, position.Bottom - outlineWidth), new Point(position.Width, outlineWidth))); // Bottom
        }

        /// <summary>
        /// Ritar ur en rektangel med texturen (texture)
        /// </summary>
        public static void DrawRect(char texture, Rectangle position)
        {
            byte convertedTexture = (byte)texture;

            //Här går vi igneom varje Y pixel
            for (int y = 0; y < position.Height; y++)
            {
                int xOffset = 0;
                //Sedan vill gå igenom alla inom X pixel
                for (int x = 0; x < position.Width; x++)
                {

                    //Dem här skapar "riktiga" pixlar, vi vill med dessa göra så pixlarna är jämna och fina.
                    //Därför lägger vi in på x axeln att vi vill skicka in 2 tecken bredvid varandra 
                    //Eftersom tecknet är dubbelt så lång som brett.
                    GraphicDevice.DrawToBuffer(convertedTexture, position.X * 2 + xOffset + x, position.Y + y);
                    GraphicDevice.DrawToBuffer(convertedTexture, position.X * 2 + xOffset + x + 1, position.Y + y);

                    // För att inte skriva över samma pixel skapar vi en varie
                    xOffset++;
                }
            }
        }

        /// <summary>
        /// Rita en bild vid den angivna positionen
        /// </summary>
        /// <param name="texture">Bilden som ska ritas</param>
        /// <param name="position">Positionen som bilden ska ritas</param>
        /// <param name="origin">Mittpunkten på bilden (normalt är det (0, 0))</param>
        /// <param name="scale"></param>
        public static void Draw(Texture2D texture, Point position, Vector2 origin = new Vector2(), float scale = 1)
        {
            //Bilden behövs inte ritas ut när scale är 0 för att den kommer ändå inte synas
            if (scale == 0)
                return;

            float newScale = 1f / scale;

            //Här går vi igneom varje Y pixel
            for (float y = 0; y < texture.Height; y += newScale)
            {
                int xOffset = 0;
                int newY = (int)(position.Y + -(scale * origin.Y) + y / newScale); //Räknar ut vart pixeln ska ritas ut på Y-ledet
                if (newY > GraphicDevice.BufferHeight) //När Y är under fönstret så kan vi returnera (avsluta updatera överhuvudtaget, gå ut ifrån den här metoden)
                    return;
                if (newY < 0) //När Y är över fönstret så kan vi hoppa ner en rad och fortsätta därifrån 
                    continue;


                //Sedan vill gå igenom alla inom X pixel
                for (float x = 0; x < texture.Width; x += newScale)
                {
                    int newX = (int)((position.X * 2f + -(scale * origin.X)) + xOffset + x / newScale); //Räknar ut vart pixeln ska ritas ut på X-ledet
                    if (newX > GraphicDevice.BufferWidth) //När X är längst till höger på fönstret så kan vi breaka (avsluta updatera X på den nuvarande Y-ledet)
                        break;
                    if (newX < 0)
                        continue;


                    int index = (int)y * texture.Width + (int)x;

                    //Dem här skapar "riktiga" pixlar, vi vill med dessa göra så pixlarna är jämna och fina.
                    //Därför lägger vi in på x axeln att vi vill skicka in 2 tecken bredvid varandra 
                    //Eftersom tecknet är dubbelt så lång som brett.
                    GraphicDevice.DrawToBuffer(texture.Pixels[index], newX, newY);
                    GraphicDevice.DrawToBuffer(texture.Pixels[index], newX + 1, newY);

                    // För att inte skriva över samma pixel skapar vi en varie
                    xOffset++;
                }
            }
        }

        public static void Draw(Texture2D texture, Point position, Rectangle source, float scale = 1f)
        {
            if (scale * Scale == 0)
                return;

            float newScale = 1f / (scale * Scale);
            position = (Point)((Vector2)position * Scale);

            //Här går vi igneom varje Y pixel
            for (float y = source.Y; y < source.Height + source.Y; y += newScale)
            {
                int xOffset = 0;
                int newY = (int)(position.Y + (y - source.Y) / newScale);
                if (newY > GraphicDevice.BufferHeight)
                    break;
                if (newY < 0)
                    continue;

                //Sedan vill gå igenom alla inom X pixel
                for (float x = source.X; x < source.Width + source.X; x += newScale)
                {
                    int newX = (int)(position.X * 2 + xOffset + (x - source.X) / newScale);

                    // För att inte skriva över samma pixel skapar vi en varie
                    xOffset++;
                    if (newX > GraphicDevice.BufferWidth)
                        break;
                    if (newX < 0)
                        continue;

                    int index = (int)y * texture.Width + (int)x;

                    //Dem här skapar "riktiga" pixlar, vi vill med dessa göra så pixlarna är jämna och fina.
                    //Därför lägger vi in på x axeln att vi vill skicka in 2 tecken bredvid varandra 
                    //Eftersom tecknet är dubbelt så lång som brett.
                    GraphicDevice.DrawToBuffer(texture.Pixels[index], newX,     newY);
                    GraphicDevice.DrawToBuffer(texture.Pixels[index], newX + 1, newY);
                }
            }
        }
    }
}
