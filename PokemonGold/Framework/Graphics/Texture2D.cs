﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Framework.Graphics
{
    // Den här klassen kommer skapa en bild
    public class Texture2D
    {
        /// <summary>
        /// All pixlar i bilden
        /// </summary>
        public byte[] Pixels;
        /// <summary>
        /// Bredden på bilden
        /// </summary>
        public int Width;
        /// <summary>
        /// Höjden på bilden
        /// </summary>
        public int Height;

        /// <summary>
        /// Returnerar storleken på bilden (Width, Height)
        /// </summary>
        public Point Size { get { return new Point(Width, Height); } }

        /// <summary>
        /// Returnerar mitten punken i bilden
        /// </summary>
        public Vector2 Center { get { return new Vector2(Width / 2, Height / 2); } }

        /// <summary>
        /// Skapar en Texture2D som är (width, height) stor och har pixlarna (pixels)
        /// </summary>
        public Texture2D(int width, int height, byte[] pixels)
        {
            Width = width;
            Height = height;
            Pixels = pixels;
        }
    }
}
