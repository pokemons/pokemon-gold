﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Framework
{
    /// <summary>
    /// Beskriver en punkt med en X och en Y
    /// </summary>
    public struct Point
    {
        /// <summary>
        /// X positionen
        /// </summary>
        public int X;
        /// <summary>
        /// Y positionen
        /// </summary>
        public int Y;

        /// <summary>
        /// Skapar en Point vid positionen (x, y)
        /// </summary>
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
        /// <summary>
        /// Skapar en Point vid positionen (x, x)
        /// </summary>
        /// 
        public Point(int x)
        {
            X = x;
            Y = x;
        }

        /// <summary>
        /// Returnerar längden på pungen (hypotenusan)
        /// </summary>
        public float Magnitude { get { return (float)Math.Sqrt(X * X + Y * Y); } }

        //Omvandlar en Vector2 till en Point
        public static explicit operator Point(Vector2 a)
        {
            return new Point((int)a.X, (int)a.Y);
        }

        /// <summary>
        /// Returnerar en ny Point med (X = 0, Y = 0)
        /// </summary>
        public static Point Zero { get { return new Point(0); } }
        /// <summary>
        /// Returnerar en ny Point med (X = 1, Y = 1)
        /// </summary>
        public static Point One { get { return new Point(1); } }
        /// <summary>
        /// Returnerar en ny Point med (X = -1, Y = 0)
        /// </summary>
        public static Point Left { get { return new Point(-1, 0); } }
        /// <summary>
        /// Returnerar en ny Point med (X = 1, Y = 0)
        /// </summary>
        public static Point Right { get { return new Point(1, 0); } }
        /// <summary>
        /// Returnerar en ny Point med (X = 0, Y = -1)
        /// </summary>
        public static Point Up { get { return new Point(0, -1); } }
        /// <summary>
        /// Returnerar en ny Point med (X = 0, Y = 1)
        /// </summary>
        public static Point Down { get { return new Point(0, 1); } }

        //Här nere lägger vi in så operator kan hantera de operatorer som vi vill att den ska hantera.
        #region operators
        // inverterar pointen
        public static Point operator -(Point a)
        {
            //Här säger man hur strukturen ska agera
            return new Point(-a.X, -a.Y);
        }


        // så vi kan addera två stycken Point klasser
        public static Point operator +(Point a, Point b)
        {
            //Här säger man hur strukturen ska agera
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        // så vi kan subtrahera två stycken Point klasser
        public static Point operator -(Point a, Point b)
        {
            //Här säger man hur strukturen ska agera
            return new Point(a.X - b.X, a.Y - b.Y);
        }

        // så vi kan multiplicera två stycken Point klasser
        public static Point operator *(Point a, Point b)
        {
            //Här säger man hur strukturen ska agera
            return new Point(a.X * b.X, a.Y * b.Y);
        }

        // så vi kan dividera två stycken Point klasser
        public static Point operator /(Point a, Point b)
        {
            //Här säger man hur strukturen ska agera
            return new Point(a.X / b.X, a.Y / b.Y);
        }

        // så vi kan addera en Point med en int
        public static Point operator +(Point a, int b)
        {
            //Här säger man hur strukturen ska agera
            return new Point(a.X + b, a.Y + b);
        }

        // så vi kan subtrahera en Point med en int
        public static Point operator -(Point a, int b)
        {
            //Här säger man hur strukturen ska agera
            return new Point(a.X - b, a.Y - b);
        }

        // så vi kan multiplicera ihop en Point med en int
        public static Point operator *(Point a, int b)
        {
            //Här säger man hur strukturen ska agera
            return new Point(a.X * b, a.Y * b);
        }

        // så vi kan dividera en Point med en int
        public static Point operator /(Point a, int b)
        {
            //Här säger man hur strukturen ska agera
            return new Point(a.X / b, a.Y / b);
        }

        // så vi kan jämnföra två stycken Point klasser
        public static bool operator ==(Point a, Point b)
        {
            //Här säger man hur strukturen ska agera
            return a.X == b.X && a.Y == b.Y;
        }
        public static bool operator !=(Point a, Point b)
        {
            //Här säger man hur strukturen ska agera
            return !(a == b);
        }
        #endregion
    }
}
