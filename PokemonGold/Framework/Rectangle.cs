﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Framework
{
    /// <summary>
    /// Beskriver en rektangel med hjälp ev en position (X, Y) och en storlek (Width, Height)
    /// </summary>
    public struct Rectangle
    {
        /// <summary>
        /// X positionen på rektangeln
        /// </summary>
        public int X;
        /// <summary>
        /// Y positionen på rektangeln
        /// </summary>
        public int Y;
        /// <summary>
        /// Bredden på rektangeln
        /// </summary>
        public int Width;
        /// <summary>
        /// Höjden på rektangeln
        /// </summary>
        public int Height;

        /// <summary>
        /// Returnerar en tom rektangel
        /// </summary>
        public static Rectangle Empty { get { return new Rectangle(); } }

        /// <summary>
        /// Returnerar TRUE om den här rektangeln är tom
        /// </summary>
        public bool IsEmpty { get { return this == Empty; } }

        /// <summary>
        /// Returnerar mitten punken i rektangeln
        /// </summary>
        public Point Center { get { return new Point(X + Width / 2, Y + Height / 2); } }
        /// <summary>
        /// Returnerar positionen på rektangeln (X, Y)
        /// </summary>
        public Point Position { get { return new Point(X, Y); } }
        /// <summary>
        /// Returnerar storleken på rektangeln (Width, Height)
        /// </summary>
        public Point Size { get { return new Point(Width, Height); } }

        /// <summary>
        /// Returnerar poitionen på den vänstra sidan av rektangeln
        /// </summary>
        public int Left { get { return X; } }
        /// <summary>
        /// Returnerar poitionen på den högra sidan av rektangeln
        /// </summary>
        public int Right { get { return X + Width; } }
        /// <summary>
        /// Returnerar den översta positionen av rektangeln
        /// </summary>
        public int Top { get { return Y; } }
        /// <summary>
        /// Returnerar den understa positionen av rektangeln
        /// </summary>
        public int Bottom { get { return Y + Height; } }

        /// <summary>
        /// Skapar en rektangel vid positionen (x, y) och är (width, height) stor
        /// </summary>
        public Rectangle(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }
        /// <summary>
        /// Skapar en rektangel vid positionen (x, y) och är (size, size) stor
        /// </summary>
        public Rectangle(int x, int y, int size)
        {
            X = x;
            Y = y;
            Width = size;
            Height = size;
        }
        /// <summary>
        /// Skapar en rektangel vid positionen (position) och är (size) stor
        /// </summary>
        public Rectangle(Point position, Point size)
        {
            X = position.X;
            Y = position.Y;
            Width = size.X;
            Height = size.Y;
        }

        public static bool operator ==(Rectangle a, Rectangle b)
        {
            return (a.Position == b.Position && a.Size == b.Size);
        }
        public static bool operator !=(Rectangle a, Rectangle b)
        {
            return !(a == b);
        }
    }
}
