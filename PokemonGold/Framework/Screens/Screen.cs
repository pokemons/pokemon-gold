﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Framework.Screens
{

    // här skapas skärmarna för menyer
    public class Screen
    {
        public Screen()
        {
            Initialize();
            LoadContent();
        }

        public virtual void Initialize() { }
        public virtual void LoadContent() { }
        public virtual void Update() { }
        public virtual void Draw() { }
    }

}
