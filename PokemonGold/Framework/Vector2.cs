﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Framework
{
    /// <summary>
    /// Beskriver en flytande punkt med en X och en Y
    /// </summary>
    public struct Vector2
    {
        /// <summary>
        /// X positionen
        /// </summary>
        public float X;
        /// <summary>
        /// Y positionen
        /// </summary>
        public float Y;

        /// <summary>
        /// Skapar en Vector2 vid positionen (x, y)
        /// </summary>
        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }
        /// <summary>
        /// Skapar en Vector2 vid positionen (x, x)
        /// </summary>
        public Vector2(float x)
        {
            X = x;
            Y = x;
        }

        /// <summary>
        /// Returnerar längden på vectorn (hypotenusan)
        /// </summary>
        public float Magnitude { get { return (float)Math.Sqrt(X * X + Y * Y); } }

        /// <summary>
        /// Returnerar riktningen för denna vektor
        /// </summary>
        public Vector2 Normalized { get { float lenth = Magnitude; return lenth == 0 ? Zero : new Vector2(X / lenth, Y / lenth); } }

        public Vector2 Normalize { get
            {
                float lenth = Magnitude;
                X /= lenth;
                Y /= lenth;

                return lenth == 0 ? Zero : this;
            } }

        //Omvandlar en Point till en Vector2
        public static explicit operator Vector2(Point a)
        {
            return new Vector2(a.X, a.Y);
        }

        /// <summary>
        /// Returnerar en ny Vector2 med (X = 0, Y = 0)
        /// </summary>
        public static Vector2 Zero { get { return new Vector2(0); } }
        /// <summary>
        /// Returnerar en ny Vector2 med (X = 1, Y = 1)
        /// </summary>
        public static Vector2 One { get { return new Vector2(1); } }
        /// <summary>
        /// Returnerar en ny Vector2 med (X = -1, Y = 0)
        /// </summary>
        public static Vector2 Left { get { return new Vector2(-1, 0); } }
        /// <summary>
        /// Returnerar en ny Vector2 med (X = 1, Y = 0)
        /// </summary>
        public static Vector2 Right { get { return new Vector2(1, 0); } }
        /// <summary>
        /// Returnerar en ny Vector2 med (X = 0, Y = -1)
        /// </summary>
        public static Vector2 Up { get { return new Vector2(0, -1); } }
        /// <summary>
        /// Returnerar en ny Vector2 med (X = 0, Y = 1)
        /// </summary>
        public static Vector2 Down { get { return new Vector2(0, 1); } }

        //Här nere lägger vi in så operator kan hantera de operatorer som vi vill att den ska hantera.
        #region operators
        // inverterar pointen
        public static Vector2 operator -(Vector2 a)
        {
            //Här säger man hur strukturen ska agera
            return new Vector2(-a.X, -a.Y);
        }


        // så vi kan addera två stycken Vector2 klasser
        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            //Här säger man hur strukturen ska agera
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }

        // så vi kan subtrahera två stycken Vector2 klasser
        public static Vector2 operator -(Vector2 a, Vector2 b)
        {
            //Här säger man hur strukturen ska agera
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }

        // så vi kan multiplicera två stycken Vector2 klasser
        public static Vector2 operator *(Vector2 a, Vector2 b)
        {
            //Här säger man hur strukturen ska agera
            return new Vector2(a.X * b.X, a.Y * b.Y);
        }

        // så vi kan dividera två stycken Vector2 klasser
        public static Vector2 operator /(Vector2 a, Vector2 b)
        {
            //Här säger man hur strukturen ska agera
            return new Vector2(a.X / b.X, a.Y / b.Y);
        }

        // så vi kan addera en Vector2 med en float
        public static Vector2 operator +(Vector2 a, float b)
        {
            //Här säger man hur strukturen ska agera
            return new Vector2(a.X + b, a.Y + b);
        }

        // så vi kan subtrahera en Vector2 med en float
        public static Vector2 operator -(Vector2 a, float b)
        {
            //Här säger man hur strukturen ska agera
            return new Vector2(a.X - b, a.Y - b);
        }

        // så vi kan multiplicera ihop en Vector2 med en float
        public static Vector2 operator *(Vector2 a, float b)
        {
            //Här säger man hur strukturen ska agera
            return new Vector2(a.X * b, a.Y * b);
        }

        // så vi kan dividera en Vector2 med en float
        public static Vector2 operator /(Vector2 a, float b)
        {
            //Här säger man hur strukturen ska agera
            return new Vector2(a.X / b, a.Y / b);
        }

        // så vi kan jämnföra två stycken Point klasser
        public static bool operator ==(Vector2 a, Vector2 b)
        {
            //Här säger man hur strukturen ska agera
            return a.X == b.X && a.Y == b.Y;
        }
        public static bool operator !=(Vector2 a, Vector2 b)
        {
            //Här säger man hur strukturen ska agera
            return !(a == b);
        }
        #endregion
    }
}
