﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Pokemon.Data
{
    [Serializable]
    public class MapData
    {
        public string Name;

        public int Width;
        public int Height;

        public string[][] Data;

        public MapData()
        {

        }

        public MapData(string name, Model.Tile[,] mapdata)
        {
            Name = name;

            Width = mapdata.GetLength(1);
            Height = mapdata.GetLength(0);

            Data = new string[Height][];
            for (int i = 0; i < Height; i++)
                Data[i] = new string[Width];

            for (int y = 0; y < Height; y++)
                for (int x = 0; x < Width; x++)
                    Data[y][x] = mapdata[y, x].Name;
        }
    }
}
