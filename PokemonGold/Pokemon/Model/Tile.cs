﻿using PokemonGold.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Pokemon.Model
{
    public class Tile
    {
        public string Name;
        public Rectangle SourceRectangle;
        public TileType Type;

        public Tile(string name,Rectangle sourceRect, TileType type)
        {
            Name = name;
            SourceRectangle = sourceRect;
            Type = type;
        }
    }

    public enum TileType
    {
        Solid,
        NotSolid,
        SemiSold
    }
}
