﻿using PokemonGold.Framework;
using PokemonGold.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Pokemon.Model
{
    public class TileMap
    {
        public static Texture2D TileSetTexture;

        public string Name;
        public Tile[,] Tiles;
        public int TileSize;

        public Point TilesInScreen;
        
        public Point Size { get => new Point(Tiles.GetLength(1), Tiles.GetLength(0)); }

        public TileMap(string name, Tile[,] tiles, int tileSize)
        {
            Name = name;
            Tiles = tiles;
            TileSize = tileSize;

            TilesInScreen = new Point(GraphicDevice.ScreenWidth  / (int)(TileSize * SpriteBatch.Scale), 
                                      GraphicDevice.ScreenHeight / (int)(TileSize * SpriteBatch.Scale)) + 
                                      Point.One;
        }

        public void Draw(Point offSet)
        {
            Point start = -offSet / TileSize;
            Point end = start + TilesInScreen;

            if (start.X < 0) start.X = 0;
            if (start.Y < 0) start.Y = 0;
            if (end.X > Size.X - 1) end.X = Size.X - 1;
            if (end.Y > Size.Y - 1) end.Y = Size.Y - 1;

            for (int y = start.Y; y < end.Y; y++)
            {
                for (int x = start.X; x < end.X; x++)
                {
                    SpriteBatch.Draw(TileSetTexture, new Point(x, y) * TileSize + offSet, Tiles[y, x].SourceRectangle);
                }
            }
        }
    }
}
