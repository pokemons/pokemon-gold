﻿using PokemonGold.Framework.Screens;
using PokemonGold.Framework.Graphics;
using PokemonGold.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonGold.Framework.Input;
using PokemonGold.Framework.Content;

namespace PokemonGold.Pokemon.Screens
{
    class BejeweledScreen : Screen
    {
        static Random Random;

        static readonly Rectangle[] Gems = {
            new Rectangle(0, 0, 32, 32),
            new Rectangle(32, 0, 32, 32),
            new Rectangle(64, 0, 32, 32),
            new Rectangle(96, 0, 32, 32),
            new Rectangle(128, 0, 32, 32),
            new Rectangle(160, 0, 32, 32),
            new Rectangle(192, 0, 32, 32)
        };
        //static readonly Rectangle Bomb = new Rectangle(224, 0, 32, 32);

        Texture2D gemsTexture;

        Rectangle[,] gemsMap;
        List<Point> gemsRemoveFlaged;
        //List<Point> bombFlaged;

        Point swapPoint;
        Point  cursorPosition;

        int totalGems;

        float delayTime;
        bool swapFail, gemsToRemove;

        GameState currentGameState, nextGameState;

        public BejeweledScreen()
        {
            //GraphicDevice.Init(352, 288);
            Program.FPS = 12;
        }

        public override void Initialize()
        {
            Random = new Random();
            currentGameState = GameState.User;
            currentGameState = GameState.User;
            totalGems = 0;

            gemsRemoveFlaged = new List<Point>();
            //bombFlaged = new List<Point>();

            //Initialize gems
            gemsMap = new Rectangle[8, 8];
            for (int y = 0; y < 8; y++)
                for (int x = 0; x < 8; x++)
                    gemsMap[y, x] = Rectangle.Empty;

            base.Initialize();
        }

        public override void LoadContent()
        {
            gemsTexture = Content.Load("..\\..\\Sprites\\jewels.png");

            base.LoadContent();
        }

        float elapsedTime = 1f;

        public override void Update() 
        {
            //if (delayTime > 0f)
            //{
            //    delayTime -= elapsedTime;
            //}
            //else
            //{
                switch (currentGameState)
                {
                    //User state : move cursor around and select items to swap
                    case GameState.User:
                        if (totalGems < 64)
                            nextGameState = GameState.Compress;
                        else
                            Input();
                        break;

                    //Swap state : swap current item with the selected item
                    case GameState.Swap:
                        swapFail = true;
                        Swap(cursorPosition, swapPoint);

                        delayTime = 0.1f; //Delay with 0.5 sec before changing state
                        nextGameState = GameState.Check;
                        break;

                    case GameState.Check:
                        Check();
                        nextGameState = GameState.Erase;
                        break;

                    case GameState.Erase:
                        if (!gemsToRemove)
                        {
                            if (swapFail)
                                Swap(cursorPosition, swapPoint);
                            
                            nextGameState = GameState.User;
                        }
                        else
                        {
                            Erase();
                            gemsToRemove = false;
                            nextGameState = GameState.Compress;
                        }
                        break;

                    case GameState.Compress:
                        Compress();

                        delayTime = 0.1f; //Delay with 0.1 sec before changing state
                        nextGameState = GameState.NewGems;
                        break;

                    case GameState.NewGems:
                        NewGems();

                        if (totalGems < 64)
                        {
                            delayTime = 0.1f;
                            nextGameState = GameState.Compress;
                        }
                        else
                            nextGameState = GameState.Check;
                        break;
                }

                currentGameState = nextGameState;
            //}

            base.Update();
        }

        public override void Draw()
        {
            //Draw all jewels
            for (int y = 0; y < 8; y++)
                for (int x = 0; x < 8; x++)
                {
                    if (gemsMap[y, x].IsEmpty) continue;

                    Point cell = new Point(x, y);
                    if (gemsRemoveFlaged.Contains(cell))
                        SpriteBatch.DrawRect('+', new Rectangle(cell * 32, new Point(32)));

                    //Rectangle sourceRect = (bombFlaged.Contains(cell) ? Bomb : gemsMap[y, x]);
                    SpriteBatch.Draw(gemsTexture, cell * 32, gemsMap[y, x]);
                }

            //Draw cursor outline
            if (totalGems == 64 && currentGameState == GameState.User)
                SpriteBatch.DrawOutlinedRect('@', new Rectangle(cursorPosition.X * 32, cursorPosition.Y * 32, 32, 32), 2);


            base.Draw();
        }

        private void Input()
        {
            if (!Keyboard.IsKeyPressed(Key.SPACE) && !Keyboard.IsKeyPressed(Key.ENTER))
            {
                //Move cursor
                if (Keyboard.IsKeyPressed(Key.UP))    cursorPosition += Point.Up;    //Move up
                if (Keyboard.IsKeyPressed(Key.DOWN))  cursorPosition += Point.Down;  //Move down
                if (Keyboard.IsKeyPressed(Key.LEFT))  cursorPosition += Point.Left;  //Move left
                if (Keyboard.IsKeyPressed(Key.RIGHT)) cursorPosition += Point.Right; //Move right

                if (Keyboard.IsKeyPressed(Key.ESCAPE)) Initialize();
            }
            else
            {
                swapPoint = cursorPosition;
                if (Keyboard.IsKeyPressed(Key.UP) && cursorPosition.Y > 0)    swapPoint.Y = cursorPosition.Y - 1;
                if (Keyboard.IsKeyPressed(Key.DOWN) && cursorPosition.Y < 7)  swapPoint.Y = cursorPosition.Y + 1;
                if (Keyboard.IsKeyPressed(Key.LEFT) && cursorPosition.X > 0)  swapPoint.X = cursorPosition.X - 1;
                if (Keyboard.IsKeyPressed(Key.RIGHT) && cursorPosition.X < 7) swapPoint.X = cursorPosition.X + 1;

                if (swapPoint.X != cursorPosition.X || swapPoint.Y != cursorPosition.Y) nextGameState = GameState.Swap;
            }

            //Clamp the cursor
            cursorPosition.X = (cursorPosition.X < 0) ? 0 : (cursorPosition.X > 7) ? 7 : cursorPosition.X;
            cursorPosition.Y = (cursorPosition.Y < 0) ? 0 : (cursorPosition.Y > 7) ? 7 : cursorPosition.Y;
        }

        private void Check()
        {
            gemsToRemove = false;

            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    if (!gemsRemoveFlaged.Contains(new Point(x, y)))
                    {
                        //bool placeBomb = false;

                        //Check Horizontally
                        int chain = 1;
                        while ((x + chain) < 8 && gemsMap[y, x] == gemsMap[y, x + chain]) chain++;
                        if (chain >= 3)
                        {
                            //if (chain >= 4) placeBomb = true;

                            while (chain > 0)
                            {
                                gemsRemoveFlaged.Add(new Point(x + chain - 1, y));
                                chain--;
                                swapFail = false;
                                gemsToRemove = true;
                            }
                        }

                        //Check Vertically
                        chain = 1;
                        while ((y + chain) < 8 && gemsMap[y, x] == gemsMap[y + chain, x]) chain++;
                        if (chain >= 3)
                        {
                            //if (chain >= 4) placeBomb = true;

                            while (chain > 0)
                            {
                                gemsRemoveFlaged.Add(new Point(x, y + chain - 1));
                                chain--;
                                swapFail = false;
                                gemsToRemove = true;
                            }
                        }

                        //if (placeBomb)
                        //{
                        //    bombFlaged.Add(new Point(x, y));
                        //    gemsRemoveFlaged.Remove(new Point(x, y));
                        //}
                    }
                }
            }
        }

        private void Compress()
        {
            for (int y = 6; y >= 0; y--)
                for (int x = 0; x < 8; x++)
                    if (!gemsMap[y, x].IsEmpty && gemsMap[y + 1, x].IsEmpty)
                    {
                        Point currentTile = new Point(x, y);
                        Swap(currentTile, currentTile + Point.Down);
                    }
        }

        private void NewGems()
        {
            //If the gem is empty; create a new, random gem
            for (int x = 0; x < 8; x++)
                if (gemsMap[0, x].IsEmpty)
                {
                    gemsMap[0, x] = Gems[Random.Next(0, 7)];
                    totalGems++;
                }
        }

        private void Erase()
        {
            for (int y = 0; y < 8; y++)
                for (int x = 0; x < 8; x++)
                    if (gemsRemoveFlaged.Contains(new Point(x, y)))
                    {
                        gemsMap[y, x] = Rectangle.Empty;
                        gemsRemoveFlaged.Remove(new Point(x, y));
                        totalGems--;
                    }
        }

        private void Swap(Point a, Point b)
        {
            Rectangle temp = gemsMap[a.Y, a.X];

            gemsMap[a.Y, a.X] = gemsMap[b.Y, b.X];
            gemsMap[b.Y, b.X] = temp;

            currentGameState = GameState.User;
        }

        enum GameState
        {
            User,
            Swap,
            Check,
            Erase,
            Compress,
            NewGems
        }
    }
}
