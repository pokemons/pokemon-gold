﻿using PokemonGold.Framework.Graphics;
using PokemonGold.Framework.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Pokemon.Screens
{
    class FailedCreatingWindowScreen : Screen
    {
        public override void Update()
        {
            Console.Clear();
            Console.WriteLine("Failed to create a window of size 320x288");
            Console.WriteLine("Try changing the font size");
            Console.WriteLine("Recommended font size is 2, this text should not be readable");

            Console.WriteLine("\nPress a key to try again..");
            Console.ReadKey();

            if (GraphicDevice.Init(320, 288))
                Utils.CurrentScreen = new GameScreen();
            else
                Utils.CurrentScreen = new FailedCreatingWindowScreen();

            base.Update();
        }
    }
}
