﻿using PokemonGold.Framework.Screens;
using PokemonGold.Framework.Graphics;
using PokemonGold.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokemonGold.Framework.Input;

namespace PokemonGold.Pokemon.Screens
{
    class Formula1Screen : Screen
    {
        float carPos = 0.0f;
        float carDirection = 0.0f;

        float distance = 0.0f;
        float trackDistance = 0.0f;

        float speed = 0.0f;
        float offSet = 0.0f;

        float targetCurvature = 0.0f;
        float trackCurvature = 0.0f;
        float playerCurvature = 0.0f;
        float curvature = 0.0f;

        int trackSection = 0;

        Texture2D carTexture, carStraightTexture, carLeftTexture, carRightTexture;

        List<Tuple<float, float>> track = new List<Tuple<float, float>>();

        public Formula1Screen()
        {

        }

        public override void Initialize()
        {
            track.Add(new Tuple<float, float>(0f, 20f));
            track.Add(new Tuple<float, float>(0f, 900f));
            track.Add(new Tuple<float, float>(-0.5f, 600f));
            track.Add(new Tuple<float, float>(1f, 600f));
            track.Add(new Tuple<float, float>(0f, 400f));
            track.Add(new Tuple<float, float>(-0.2f, 450f));
            track.Add(new Tuple<float, float>(-1f, 600f));
            track.Add(new Tuple<float, float>(1f, 600f));
            track.Add(new Tuple<float, float>(1f, 500f));
            track.Add(new Tuple<float, float>(0f, 450f));
            track.Add(new Tuple<float, float>(-1f, 400f));
            track.Add(new Tuple<float, float>(0f, 600f));
            track.Add(new Tuple<float, float>(-1f, 550f));
            track.Add(new Tuple<float, float>(0.3f, 500f));
            track.Add(new Tuple<float, float>(0f, 400f));

            foreach (var curve in track)
                trackDistance += curve.Item2;

            base.Initialize();
        }

        public override void LoadContent()
        {
            char[] carStraightPixels =
            {
                ' ', ' ' , ' ', '|', '|', '#' , '#', '#', '#', '|' , '|', ' ', ' ', ' ',
                ' ', ' ' , ' ', ' ', ' ', ' ' , '#', '#', ' ', ' ' , ' ', ' ', ' ', ' ',
                ' ', ' ' , ' ', ' ', ' ', '#' , '#', '#', '#', ' ' , ' ', ' ', ' ', ' ',
                ' ', ' ' , ' ', ' ', ' ', '#' , '#', '#', '#', ' ' , ' ', ' ', ' ', ' ',
                '|', '|' , '|', ' ', ' ', '#' , '#', '#', '#', ' ' , ' ', '|', '|', '|',
                '|', '|' , '|', '#', '#', '#' , '#', '#', '#', '#' , '#', '|', '|', '|',
                '|', '|' , '|', ' ', ' ', '#' , '#', '#', '#', ' ' , ' ', '|', '|', '|'
            };
            char[] carLeftPixels =
            {
                '|', '|', '#', '#', '#', '#', '|', '|', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', '#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', ' ', ' ', ' ', '#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ',
                ' ', '|', '|', '|', '|', '#', '#', '#', '#', ' ', ' ', '|', '|', '|',
                ' ', 'R', '|', '|', '|', '#', '#', '#', '#', '#', '#', '#', '|', '|',
                ' ', '|', '|', '|', '|', ' ', '#', '#', '#', '#', ' ', '|', '|', '|',
            };
            char[] carRightPixels =
            {
                ' ', ' ' , ' ', ' ', ' ', ' ' , '|', '|', '#', '#' , '#', '#', '|', '|',
                ' ', ' ' , ' ', ' ', ' ', ' ' , ' ', ' ', ' ', '#' , '#', ' ', ' ', ' ',
                ' ', ' ' , ' ', ' ', ' ', ' ' , ' ', '#', '#', '#' , '#', ' ', ' ', ' ',
                ' ', ' ' , ' ', ' ', ' ', ' ' , '#', '#', '#', '#' , ' ', ' ', ' ', ' ',
                '|', '|' , '|', ' ', ' ', '#' , '#', '#', '#', '|' , '|', '|', '|', ' ',
                '|', '|' , '#', '#', '#', '#' , '#', '#', '#', '|' , '|', '|', 'R', ' ',
                '|', '|' , '|', ' ', '#', '#' , '#', '#', ' ', '|' , '|', '|', '|', ' '
            };

            carStraightTexture = new Texture2D(14, 7, Encoding.UTF8.GetBytes(carStraightPixels));
            carLeftTexture     = new Texture2D(14, 7, Encoding.UTF8.GetBytes(carLeftPixels));
            carRightTexture    = new Texture2D(14, 7, Encoding.UTF8.GetBytes(carRightPixels));

            carTexture = carStraightTexture;

            base.LoadContent();
        }

        float elapsedTime = 0.04f;

        public override void Update()
        {
            //Accelaration
            if (Keyboard.IsKeyPressed(Key.UP))
                speed += 2f * elapsedTime;
            else
                speed -= 1f * elapsedTime;

            carDirection = 0f;
            //Steering
            if (Keyboard.IsKeyPressed(Key.LEFT))
            {
                playerCurvature -= 0.7f * elapsedTime;
                carDirection += -1f;
            }
            if (Keyboard.IsKeyPressed(Key.RIGHT))
            {
                playerCurvature += 0.7f * elapsedTime;
                carDirection += 1f;
            }

            //Slowdown car, car is outside track
            //if (Math.Abs(playerCurvature * 2 - trackCurvature) >= 0.8f)
              //  speed -= 6f * elapsedTime;

            //Clamp
            if (speed < 0f) speed = 0f;
            if (speed > 1f) speed = 1f;

            //Move car along track according to car speed
            distance += (500f * speed) * elapsedTime;

            offSet = 0;
            trackSection = 0;

            if (distance >= trackDistance)
                distance -= trackDistance;

            while (trackSection < track.Count && offSet <= distance)
            {
                offSet += track[trackSection].Item2;
                trackSection++;
            }

            targetCurvature = track[trackSection - 1].Item1;

            float trackCurvDiff = (targetCurvature - curvature) * elapsedTime * speed;
            curvature += trackCurvDiff;

            trackCurvature += curvature * elapsedTime * speed;

            base.Update();
        }

        public override void Draw()
        {
            //Draw sky
            SpriteBatch.DrawRect('W', new Rectangle(0, 0, GraphicDevice.ScreenWidth, GraphicDevice.ScreenHeight / 2));
            SpriteBatch.DrawRect('#', new Rectangle(0, 0, GraphicDevice.ScreenWidth, (int)(GraphicDevice.ScreenHeight / 2.5f)));
            SpriteBatch.DrawRect('P', new Rectangle(0, 0, GraphicDevice.ScreenWidth, GraphicDevice.ScreenHeight / 4));

            //Draw hills
            for (int x = 0; x < GraphicDevice.ScreenWidth; x++)
            {
                int hillHeight = (int)(Math.Abs(Math.Sin(x * 0.01f + trackCurvature * 4) * 30f));
                for (int y = GraphicDevice.ScreenHeight / 2 - hillHeight; y < GraphicDevice.ScreenHeight / 2; y++)
                {
                    SpriteBatch.DrawPixel('Z', new Point(x, y));
                }
            }

            //Draw road
            for (int y = 0; y < GraphicDevice.ScreenHeight / 2; y++)
            {
                for (int x = 0; x < GraphicDevice.ScreenWidth; x++)
                {
                    float perspective = y / (GraphicDevice.ScreenHeight / 2f);

                    float middlePoint = 0.5f + curvature * (float)Math.Pow((1f - perspective), 3);
                    float roadWidth = 0.1f + perspective * 0.8f;
                    float clipWidth = roadWidth * 0.15f;

                    roadWidth *= 0.5f;

                    int leftGrass  = (int)((middlePoint - roadWidth  - clipWidth) * GraphicDevice.ScreenWidth);
                    int leftClip   = (int)((middlePoint - roadWidth) * GraphicDevice.ScreenWidth);
                    int rightGrass = (int)((middlePoint + roadWidth  + clipWidth) * GraphicDevice.ScreenWidth);
                    int rightClip  = (int)((middlePoint + roadWidth) * GraphicDevice.ScreenWidth);

                    int row = GraphicDevice.ScreenHeight / 2 + y;

                    char grassColor = Math.Sin(20f * Math.Pow(1f - perspective, 3) + distance * 0.1f) > 0f ? '+' : 'x';
                    char clipColor  = Math.Sin(80f * Math.Pow(1f - perspective, 3) + distance * 0.1f) > 0f ? '*' : ' ';
                    char roadColor = (trackSection - 1) == 0 ? '.' : '@';

                    Point position = new Point(x, row);

                    if (x >= 0 && x < leftGrass)
                        SpriteBatch.DrawPixel(grassColor, position);
                    else if (x >= leftGrass && x < leftClip)
                        SpriteBatch.DrawPixel(clipColor, position);
                    else if (x >= leftClip && x < rightClip)
                        SpriteBatch.DrawPixel(roadColor, position);
                    else if (x >= rightClip && x < rightGrass)
                        SpriteBatch.DrawPixel(clipColor, position);
                    else if (x >= rightGrass && x < GraphicDevice.ScreenWidth)
                        SpriteBatch.DrawPixel(grassColor, position);
                }
            }

            //Draw car
            carPos = playerCurvature - trackCurvature;
            float newCarPos = GraphicDevice.ScreenWidth / 2 + (GraphicDevice.ScreenWidth * carPos * 2f) / 2f - 7; 

            switch (carDirection)
            {
                case 0:
                    carTexture = carStraightTexture;
                    break;

                case -1:
                    carTexture = carLeftTexture;
                    break;

                case 1:
                    carTexture = carRightTexture;
                    break;
            }
            Point carPosition = new Point((int)newCarPos, (int)(GraphicDevice.ScreenHeight * 0.8f));
            SpriteBatch.Draw(carTexture, carPosition, carTexture.Center, 2f);

            base.Draw();
        }
    }
}