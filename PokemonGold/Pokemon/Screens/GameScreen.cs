﻿using PokemonGold.Framework.Screens;
using PokemonGold.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using PokemonGold.Framework.Graphics;
using PokemonGold.Pokemon.Model;
using PokemonGold.Framework;
using PokemonGold.Framework.Input;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using PokemonGold.Pokemon.Data;

namespace PokemonGold.Pokemon.Screens
{
    class GameScreen : Screen
    {
        TileMap map1;
        Point offSet;

        public GameScreen()
        {
            //Läser in en TileMap
            //XmlSerializer serializer = new XmlSerializer(typeof(MapData));
            //using (FileStream fs = new FileStream("testXml.xml", FileMode.Open, FileAccess.Read))
            //{
            //    MapData map = serializer.Deserialize(fs) as MapData;
            //}

            //Skapa en TileMap
            //XmlSerializer serializer = new XmlSerializer(typeof(MapData));
            //using (FileStream fs = new FileStream("testXml.xml", FileMode.Create, FileAccess.Write))
            //{
            //    serializer.Serialize(fs, new MapData("Map1", tiles));
            //}

            //Dictionary<string, object> data = FileReader.Read("..\\..\\Parser\\DataFile.txt");
            //string a = ((object[])data["Objects"])[0] as string;
        }

        public override void Initialize()
        {
            SpriteBatch.Scale = 2f;

            base.Initialize();
        }

        public override void LoadContent()
        {
            TileMap.TileSetTexture = Content.Load(@"..\..\Sprites\tileSetGold.png");

            Tile tile = new Tile("grass", new Rectangle(52, 52, 16, 16), TileType.NotSolid);
            Tile[,] tiles = new Tile[20, 30];

            for (int y = 0; y < 20; y++)
                for (int x = 0; x < 30; x++)
                    tiles[y, x] = tile;

            map1 = new TileMap("", tiles, 16);

            base.LoadContent();
        }

        public override void Update()
        {
            offSet -= 1;

            base.Update();
        }

        public override void Draw()
        {
            map1.Draw(offSet);

            base.Draw();
        }
    }
}
