﻿using PokemonGold.Framework;
using PokemonGold.Framework.Content;
using PokemonGold.Framework.Graphics;
using PokemonGold.Framework.Input;
using PokemonGold.Framework.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonGold.Pokemon.Screens
{
    class SnakeScreen : Screen
    {
        Texture2D headTexture, bodyTexture, foodTexture;

        Random random;
        Point velocity, headPosition, foodPosition;
        List<Point> bodyPositions;

        bool dead;

        public SnakeScreen()
        {
                
        }

        private void SetFoodPosition()
        {
            //24, 16

            foodPosition = new Point(random.Next(24), random.Next(16)) * 20;
        }

        public override void Initialize()
        {
            random = new Random();

            dead = false;
            velocity = Point.Zero;
            headPosition = new Point(240, 160);
            bodyPositions = new List<Point>();

            SetFoodPosition();


            base.Initialize();
        }

        public override void LoadContent()
        {
            headTexture = Content.Load(@"..\..\Sprites\head.png");
            bodyTexture = Content.Load(@"..\..\Sprites\\body.png");
            foodTexture = Content.Load(@"..\..\Sprites\\star.png");

            base.LoadContent();
        }

        private void CheckIfDead()
        {
           foreach (Point body in bodyPositions)
            {
                if (body == headPosition)
                {
                    velocity = Point.Zero;
                    dead = true;
                    return;
                }
            }

            Point newPosition = velocity * 20 + headPosition;
            if (newPosition.X < 0 || newPosition.X > GraphicDevice.ScreenWidth  - 20 ||
                newPosition.Y < 0 || newPosition.Y > GraphicDevice.ScreenHeight - 20)
            {
                velocity = Point.Zero;
                if (bodyPositions.Count > 0) dead = true;
                return;
            }
        }

        public override void Update()
        {
            if (!dead)
            {
                if (Keyboard.IsKeyPressed(Key.UP) && velocity != Point.Down)
                    velocity = Point.Up;
                else if (Keyboard.IsKeyPressed(Key.DOWN) && velocity != Point.Up)
                    velocity = Point.Down;
                else if (Keyboard.IsKeyPressed(Key.LEFT) && velocity != Point.Right)
                    velocity = Point.Left;
                else if (Keyboard.IsKeyPressed(Key.RIGHT) && velocity != Point.Left)
                    velocity = Point.Right;

                CheckIfDead();

                if (bodyPositions.Count > 0 && !dead)
                {
                    bodyPositions.RemoveAt(0);
                    bodyPositions.Add(headPosition);
                }

                if (headPosition == foodPosition)
                {
                    bodyPositions.Insert(0, headPosition);


                    SetFoodPosition();
                }

                headPosition += velocity * 20;
            }
            else
            {
                if (Keyboard.IsKeyPressed(Key.SPACE))
                    Initialize();
            }
            base.Update();
        }

        public override void Draw()
        {
            SpriteBatch.Draw(foodTexture, foodPosition);

            foreach (Point body in bodyPositions)
                SpriteBatch.Draw(bodyTexture, body);

            SpriteBatch.Draw(headTexture, headPosition);

            if (dead)
            {
                for (int i = 0; i < bodyPositions.Count; i++)
                {
                    SpriteBatch.Draw(headTexture, new Point(GraphicDevice.ScreenCenter.X - bodyPositions.Count / 2 * 20 + i * 20, 60));
                }
            }

            base.Draw();
        }
    }
}
