﻿using PokemonGold.Framework;
using PokemonGold.Framework.Graphics;
using PokemonGold.Framework.Input;
using PokemonGold.Pokemon;
using PokemonGold.Pokemon.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PokemonGold
{
    class Program
    {
        public static int FPS = 100;

        static void Main(string[] args)
        {
            Console.CursorVisible = false;

            // GameBoys skärmupplösning är 160x144, vi gör allt dubbelt så stort för att annars blir det för smått
            if (GraphicDevice.Init(480, 320)) // Skapa ett fönster som är 320x288
                Utils.CurrentScreen = new GameScreen();
            else
                Utils.CurrentScreen = new FailedCreatingWindowScreen();

            LoadContent();

            //Gameloop
            while (true)
            {
                Update();

                Draw();

                //Här lägger vi in hur många fps vi vill ha. 
                Thread.Sleep(1000 / FPS);
            }
        }

        private static void LoadContent()
        {
            // Här är färgen vi vill ha ! perfekto
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;


            //HEJSAN ALLIHOPA   
            //FACEPALM
            //VAD NU??
            //jävla FUCKING GITHUB HARD

            //Utils.CurrentScreen = new SnakeScreen();
            //Utils.CurrentScreen = new GameScreen();
        }


        private static void Update()
        {
            Utils.CurrentScreen.Update();
        }

        private static void Draw()
        {
            GraphicDevice.ClearBuffer();

            Utils.CurrentScreen.Draw();

            //den här metdeon tar allting ifrån buffern och lägger in till skärmen
            GraphicDevice.Render();
        }
    }
}